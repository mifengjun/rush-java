package org.lvgo.rushjava.medium;

//给定一个不含重复元素的整数数组。一个以此数组构建的最大二叉树定义如下：
//
//
// 二叉树的根是数组中的最大元素。
// 左子树是通过数组中最大值左边部分构造出的最大二叉树。
// 右子树是通过数组中最大值右边部分构造出的最大二叉树。
//
//
// 通过给定的数组构建最大二叉树，并且输出这个树的根节点。
//
//
//
// 示例 ：
//
// 输入：[3,2,1,6,0,5]
//输出：返回下面这棵树的根节点：
//
//      6
//    /   \
//   3     5
//    \    /
//     2  0
//       \
//        1
//
//
//
//
// 提示：
//
//
// 给定的数组的大小在 [1, 1000] 之间。

/**
 * No654
 * <p>
 * 欢迎跟我一起学习，微信（lvgocc）公众号：星尘的一个朋友
 *
 * @author lvgorice@gmail.com
 * @version 1.0
 * @blog @see http://lvgo.org
 * @CSDN @see https://blog.csdn.net/sinat_34344123
 * @date 2021/1/2
 */
public class No654 {

    public static void main(String[] args) {
        No654 no654 = new No654();
        no654.constructMaximumBinaryTree(new int[]{3, 2, 1, 6, 0, 5});
    }

    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return constructMaximumBinaryTreeChild(nums, 0, nums.length - 1);
    }

    TreeNode constructMaximumBinaryTreeChild(int[] nums, int start, int end) {
        if (start > end) {
            return null;
        }
        System.out.printf("Start: %s End: %s\n", start, end);

        int maximum = Integer.MIN_VALUE;
        int index = -1;
        for (int i = start; i <= end; i++) {
            if (nums[i] > maximum) {
                index = i;
                maximum = nums[i];
            }
        }
        System.out.println(maximum);
        TreeNode root = new TreeNode(maximum);
        root.left = constructMaximumBinaryTreeChild(nums, start, index - 1);
        root.right = constructMaximumBinaryTreeChild(nums, index + 1, end);
        return root;
    }

    private class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}

