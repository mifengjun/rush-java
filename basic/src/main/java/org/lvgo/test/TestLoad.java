package org.lvgo.test;

/**
 * TestLoad
 * <p>
 * 欢迎跟我一起学习，微信（lvgocc）公众号：星尘的一个朋友
 *
 * @author lvgorice@gmail.com
 * @version 1.0
 * @blog @see http://lvgo.org
 * @CSDN @see https://blog.csdn.net/sinat_34344123
 * @date 2021/1/17
 */
public class TestLoad {
    public static void main(String[] args) {
        B b = new B();
    }
}

class A {
    static {
        System.out.println("A");
    }
    public A() {
        System.out.println("a");
    }
}

class  B extends A {
    static {
        System.out.println("B");
    }
    public B() {
        System.out.println("b");
    }
}
