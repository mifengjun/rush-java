package org.lvgo.collect;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * TestListAllocation
 * <p>
 * 微信（lvgocc）公众号：星尘的一个朋友
 * <p>
 * 作者：米啊
 *
 * @author lvgorice@gmail.com
 * @version 1.0
 * @blog @see http://lvgo.org
 * @CSDN @see https://blog.csdn.net/sinat_34344123
 * @date 2021/3/26
 */
public class TestListAllocation {

    public static void main(String[] args) throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(10);
        ArrayList<TestC> testCS = new ArrayList<>();
        for (int i = 0; i < 20; i++) {

            TimeUnit.SECONDS.sleep(1);

            testCS.add(new TestC());
        }
    }

}

class TestC {
    private byte[] bytes = new byte[1024 * 1024];
}
