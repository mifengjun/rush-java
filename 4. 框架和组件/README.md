# 4. 框架和组件

## [4.1 Spring](./4.%20框架和组件/4.1%20Spring)

## [4.2 MyBatis](./4.%20框架和组件/4.2%20MyBatis)

## [4.3 Netty⭐](4.%20框架和组件/4.3%20Netty⭐)

## [4.4 RocketMQ⭐](4.%20框架和组件/4.4%20RocketMQ⭐)

## [4.5 Dubbo⭐](4.%20框架和组件/4.5%20Dubbo⭐)

## 4.6 综合

### Kafka、ActiveMQ、RabbitMQ、RocketMQ 都有什么优点和缺点？