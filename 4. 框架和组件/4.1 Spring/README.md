# Spring Boot

## springboot启动过程，tomcat在什么时候启动

## springboot自动装配原理


# spring cloud

## 简述
spring cloud 由多个子项目构成，为了和子项目版本区别开，spring cloud的每个版本使用伦敦地铁站命名
根据首字母顺序来对应版本发布时间顺序。

当发布内容足够多时，或解决重大问题，发布一个   Services Releases 版本简称 SR ，通常后面会跟一个数字，代表当前版本的第几个发布版本

目前 spring 官网最新发布的 spring cloud 是 

```text
Release Train Version: Hoxton.SR9

Supported Boot Version: 2.3.5.RELEASE
```

spring cloud 每个版本都会对应支持一个 spring boot 版本。

## 子项目

### config

### netflix

### bus

### consul

### security

### sleuth

### stream

### task

### zookeeper

### gateway

### openfeign

----

## 微服务架构体系

### 基础设施
#### config
#### netflix
#### bus
#### stream

----

### 网关
#### gateway
#### Zuul

----

### 服务治理
#### zookeeper
#### Eureka
#### consul
#### openfeign

----

### 微服务安全
#### security

----

### 服务链路跟踪
#### sleuth
#### Zipkin

----

### 消息驱动开发组件
#### Kafka
#### activemq
#### rabbitmq
#### rocketmq

### task

----

### 应急预案——服务降级
#### hystrix
