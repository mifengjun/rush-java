# 注册中心宕机之后, dubbo 还能调用吗?

可以的, 因为 dubbo 的消费者在启动时会保存生产者的服务信息. 每次调用的时候会根据本地数据进行调用.

# dubbo 的工作原理

1. 生产者启动时将服务信息注册在注册中心
2. 消费者启动时订阅生产者提供的服务列表,当生产者信息发生变化时通知给订阅的消费者
3. 消费者根据注册中心提供的生产者信息进行服务调用.
4. 调用的数据在 dubbo 的监控中心可以查询的到.

调用关系说明
1. 服务容器负责启动，加载，运行服务提供者。
2. 服务提供者在启动时，向注册中心注册自己提供的服务。
3. 服务消费者在启动时，向注册中心订阅自己所需的服务。
4. 注册中心返回服务提供者地址列表给消费者，如果有变更，注册中心将基于长连接推送变更数据给消费者。
5. 服务消费者，从提供者地址列表中，基于软负载均衡算法，选一台提供者进行调用，如果调用失败，再选另一台调用。
6. 服务消费者和提供者，在内存中累计调用次数和调用时间，定时每分钟发送一次统计数据到监控中心。

# dubbo 在注册中心存了哪些信息?
1. 接口的全部限定名
2. 生产者目录/消费者目录
3. 生产者注册信息
    - dubbo://192.168.30.32:20880/org.example.DemoService?anyhost=true&application=hello-world-app&bean.name=org.example.DemoService&dubbo=2.0.2&generic=false&interface=org.example.DemoService&methods=sayHello&pid=14760&release=2.7.0&side=provider&timestamp=1614068937067, dubbo version: 2.7.0, current host: 192.168.30.32
    - provider://192.168.30.32:20880/org.example.DemoService?anyhost=true&application=hello-world-app&bean.name=org.example.DemoService&bind.ip=192.168.30.32&bind.port=20880&category=configurators&check=false&dubbo=2.0.2&generic=false&interface=org.example.DemoService&methods=sayHello&pid=14760&release=2.7.0&side=provider&timestamp=1614068937067, dubbo version: 2.7.0, current host: 192.168.30.32
IP, dubbo 端口,全部限定名,应用名称,dubbo 版本信息,接口名,方法名,进程id,时间戳

# dubbo 默认使用 multicast 广播注册中心暴露服务地址

# dubbo 如何区分重载方法?

[https://dubbo.apache.org/zh/docs/v2.7/user/examples/](https://dubbo.apache.org/zh/docs/v2.7/user/examples/)

# 启动时检查
在启动时检查依赖的服务是否可用, 默认 `check=true`

集群容错
集群调用失败时，Dubbo 提供的容错方案

负载均衡
Dubbo 提供的集群负载均衡策略

线程模型
配置 Dubbo 中的线程模型

直连提供者
Dubbo 中点对点的直连方式

只订阅
只订阅不注册

多协议
在 Dubbbo 中配置多协议

多注册中心
在 Dubbo 中把同一个服务注册到多个注册中心上

服务分组
使用服务分组区分服务接口的不同实现

静态服务
将 Dubbo 服务标识为非动态管理模式

多版本
在 Dubbo 中为同一个服务配置多个版本

分组聚合
通过分组聚合按组合并返回结果

参数验证
在 Dubbo 中进行参数验证

结果缓存
通过缓存结果加速访问速度

使用泛化调用
实现一个通用的服务测试框架，可通过 GenericService 调用所有服务实现

Protobuf
使用 IDL 定义服务

GoogleProtobuf 对象泛化调用
对 Google Protobuf 对象进行泛化调用

实现泛化调用
实现一个通用的远程服务 Mock 框架，可通过实现 GenericService 接口处理所有服务请求

回声测试
通过回声测试检测 Dubbo 服务是否可用

上下文信息
通过上下文存放当前调用过程中所需的环境信息

隐式参数
通过 Dubbo 中的 Attachment 在服务消费方和提供方之间隐式传递参数

异步执行
Dubbo 服务提供方的异步执行

异步调用
在 Dubbo 中发起异步调用

本地调用
在 Dubbo 中进行本地调用

参数回调
通过参数回调从服务器端调用客户端逻辑

事件通知
在调用之前、调用之后、出现异常时的时间通知

本地存根
在 Dubbo 中利用本地存根在客户端执行部分逻辑

本地伪装
如何在 Dubbo 中利用本地伪装实现服务降级

延迟暴露
延迟暴露 Dubbo 服务

并发控制
Dubbo 中的并发控制

连接控制
Dubbo 中服务端和客户端的连接控制

延迟连接
在 Dubbo 中配置延迟连接

粘滞连接
为有状态服务配置粘滞连接

TLS
通过 TLS 保证传输安全

令牌验证
通过令牌验证在注册中心控制权限

路由规则
通过 Dubbo 中的路由规则做服务治理

旧路由规则
在 Dubbo 2.6.x 版本以及更早的版本中配置路由规则

配置规则
在 Dubbo 中配置应用级治理规则和服务级治理规则

旧配置规则
Dubbo 中旧版本的规则配置方式

服务降级
降级 Dubbo 服务

消费端线程池模型
Dubbo 消费端线程池模型用法

优雅停机
让 Dubbo 服务完成优雅停机

主机绑定
在 Dubbo 中绑定主机名

主机配置
自定义 Dubbo 服务对外暴露的主机地址

注册信息简化
减少注册中心上服务的注册数据

日志适配
在 Dubbo 中适配日志框架

访问日志
配置 Dubbo 的访问日志

服务容器
使用 Dubbo 中的服务容器

ReferenceConfig 缓存
在 Dubbo 中缓存 ReferenceConfig

只注册
只注册不订阅

分布式事务
Dubbo 中分布式事务的支持

导出线程堆栈
在 Dubbo 自动导出线程堆栈来保留现场

Kryo 和 FST 序列化
在 Dubbo 中使用高效的 Java 序列化（Kryo 和 FST）

