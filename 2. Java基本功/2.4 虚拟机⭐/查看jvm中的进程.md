**查看jvm中的进程**

```text
jps -l
2480 org.jetbrains.idea.maven.server.RemoteMavenServer
13304 jdk.jcmd/sun.tools.jps.Jps
4728 org.apache.catalina.startup.Bootstrap
```

左边为pid，右边为进程名



**查看class加载统计**

```text
jstat -class 4728
Loaded  Bytes  Unloaded  Bytes     Time
  3025  6182.0        0     0.0       1.35
```

说明

Loaded：加载class的数量

Bytes：所占用空间大小

Unloaded：未加载数量

Bytes：未加载占用空间

Time：时间



**查看编译统计**

```text
jstat -compiler 4728
Compiled Failed Invalid   Time   FailedType FailedMethod
    2595      0       0     4.79          0
```

说明

Compiled：编译数量。

Failed：失败数量

Invalid：不可用数量

Time：时间

FailedType：失败类型

FailedMethod：失败的方法



**打印GC情况（每1秒中打印一次，共打印5次）**

```text
jstat -gc 4728 1000 5
 S0C    S1C    S0U    S1U      EC       EU        OC         OU       MC     MU    CCSC   CCSU   YGC     YGCT    FGC    FGCT     GCT
 0.0   3072.0  0.0   3072.0 51200.0  31744.0   74752.0     8693.5   21504.0 20116.8 2304.0 1985.4      3    0.022   0      0.000    0.022
 0.0   3072.0  0.0   3072.0 51200.0  31744.0   74752.0     8693.5   21504.0 20116.8 2304.0 1985.4      3    0.022   0      0.000    0.022
 0.0   3072.0  0.0   3072.0 51200.0  31744.0   74752.0     8693.5   21504.0 20116.8 2304.0 1985.4      3    0.022   0      0.000    0.022
 0.0   3072.0  0.0   3072.0 51200.0  31744.0   74752.0     8693.5   21504.0 20116.8 2304.0 1985.4      3    0.022   0      0.000    0.022
 0.0   3072.0  0.0   3072.0 51200.0  31744.0   74752.0     8693.5   21504.0 20116.8 2304.0 1985.4      3    0.022   0      0.000    0.022
```

说明

S0C：第一个Survivor区的大小（KB）

S1C：第二个Survivor区的大小（KB）

S0U：第一个Survivor区的使用大小（KB）

S1U：第二个Survivor区的使用大小（KB）

EC：Eden区的大小（KB）

EU：Eden区的使用大小（KB）

OC：Old区大小（KB）

OU：Old使用大小（KB）

MC：方法区大小（KB）

MU：方法区使用大小（KB）

CCSC：压缩类空间大小（KB）

CCSU：压缩类空间使用大小（KB）

YGC：年轻代垃圾回收次数

YGCT：年轻代垃圾回收消耗时间

FGC：老年代垃圾回收次数

FGCT：老年代垃圾回收消耗时间

GCT：垃圾回收消耗总时间

![img](https://i.loli.net/2021/03/26/rapo7lTIKBjCMLn.jpg)

前面通过jstat可以对jvm堆的内存进行统计分析，而jmap可以获取到更加详细的内容，如：内存使用情况的汇总、对内存溢出的定位与分析。

**查看内存使用情况**

jmap ‐heap 4728

**#查看所有对象，包括活跃以及非活跃的**

jmap ‐histo <pid> | more

\#查看活跃对象

jmap ‐histo:live <pid> | more

![img](https://i.loli.net/2021/03/26/JqBvbfMTceAHRd1.jpg)

\#对象说明

B byte

C char

D double

F float

I int

J long

Z boolean

[ 数组，如[I表示int[]

[L+类名 其他对象

**将内存使用情况dump到文件中**

jmap -dump:format=b,file=dump_tomcat_4728.dat 4728

![img](https://i.loli.net/2021/03/26/swZWcxbe7MCoUzf.jpg)

**通过jhat对dump文件进行分析**

jhat ‐port <port> <file>

jhat -port 9999 dump_tomcat_4728.dat

![img](https://i.loli.net/2021/03/26/4xVRNu6SwAsgZnQ.jpg)

![img](https://i.loli.net/2021/03/26/zjQE3vefSp2rZuG.jpg)

还可以使用查询语句查询

![img](https://pic1.zhimg.com/80/v2-7206be38e57f5ec5012840834b77118c_720w.jpg)

**使用MAT工具对dump文件进行分析**

MAT(Memory Analyzer Tool)，一个基于Eclipse的内存分析工具，是一个快速、功能丰

富的JAVA heap分析工具，它可以帮助我们查找内存泄漏和减少内存消耗。

![img](https://i.loli.net/2021/03/26/iI4At975cwUxrnu.jpg)